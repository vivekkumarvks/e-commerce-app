import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import ProductServices from "../services/ProductServices";

function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleUsername = (event) => {
    setUsername(event.target.value);
  };

  const handlePassword = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let body = { username, password };
    ProductServices.Login(body);
    navigate("/");
  };

  let form = (
    <>
      <form onSubmit={handleSubmit}>
        <div flex>
          <input
            type="text"
            placeholder="username"
            onClick={handleUsername}
            flex
          />
        </div>
        <div>
          <input
            type="password"
            placeholder="enter password"
            onClick={handlePassword}
          />
        </div>
        <button
          className="btn btn-secondary"
          type="submit"
          onClick={handleSubmit}
        >
          Login
        </button>
      </form>
    </>
  );
  return (
    <div className="login">
      <div className="login-block"></div>
      <div className="login-page">
        <h3>Please Login</h3> {form}
      </div>
      <div className="login-block"></div>
    </div>
  );
}

export default Login;
